class UsersController < ApplicationController

  before_action :authenticate_user!

  # Gets a list of suggested users to follow. 
  def index
    if current_user.followings.count == 0
      users = User.limit(10)
    else
      users = current_user.unfollowed_users
    end
    if users
      render json: {
        success: true,
        data: users
      }, status: 200
    end
  end

  # Returns a list of users followed by the current user
  def followings
    # user = User.find(params[:id])
    users = current_user.followings
    render json: {
      success: true,
      data: users
    }, status: 200
  end

  # Returns the current user's followers
  def followers
    users = current_user.followers
    render json: {
      success: true,
      data: users
    }, status: 200
  end

  # Returns the number of tweets, followings and followers of the current user
  def stats
    stats = {
      tweets: current_user.tweets.count,
      followings: current_user.followings.count,
      followers: current_user.followers.count
    }
    render json: {
      success: true,
      data: stats
    }, status: 200
  end
end
