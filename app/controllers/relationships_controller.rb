class RelationshipsController < ApplicationController

  before_action :authenticate_user!

  # This method creates a follow relationship
  # The actual follow is done by the follow method in the user model
  def create
    # current_user.following_relationships.build(following_id: params[:following_id])
    user = User.find(params[:following_id])
    current_user.follow(user)
    render json: {
      success: true,
      data: true
    }, status: 200
  end

  # This method is used to unfollow a user. 
  # The actual unfollow is done by the unfollow method in the user model
  def destroy
    user = current_user.following_relationships.find_by(following_id: params[:id]).following
    # user = Relationship.find(params[:id]).following
    current_user.unfollow(user)
    render json: {
      success: true,
      data: false
    }, status: 200
  end
end
