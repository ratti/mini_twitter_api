class TweetsController < ApplicationController

  before_action :authenticate_user!

  # Returns tweets of the user and those he follows in descending order.
  # It calls the feed method on the user
  def index
    tweets = current_user.feed.recent_first
    render json: {
      success: true,
      data: tweets
    }
  end

  # Returns a user's tweets only
  def user_tweets
    tweets = current_user.tweets.recent_first
    render json: {
      success: true,
      data: tweets
    }
  end

  # Save tweets
  def create
    tweet = current_user.tweets.build(tweet_params)
    if tweet.save
      response = {
        success: true,
        data: tweet
      }
    else
      response = {
        success: false,
        errors: tweet.errors.full_messages
      }
    end

    render json: response
  end

  private

  def tweet_params
    params.require(:tweet).permit(:content)
  end
end
