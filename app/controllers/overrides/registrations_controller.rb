module Overrides
  class RegistrationsController < DeviseTokenAuth::RegistrationsController

    def render_create_success
      render json: {
        success: true,
        data: resource_data
      }
    end
  end
end
