class User < ActiveRecord::Base
  # Include default devise modules.
  devise :database_authenticatable, :registerable,
          :recoverable, :rememberable, :trackable, :validatable,
          :omniauthable
          
  include DeviseTokenAuth::Concerns::User

  has_many :tweets, dependent: :destroy
  has_many :following_relationships, class_name: "Relationship", foreign_key: "follower_id", dependent: :destroy
  has_many :followings, through: :following_relationships
  has_many :follower_relationships, class_name: "Relationship", foreign_key: "following_id", dependent: :destroy
  has_many :followers, through: :follower_relationships

  # Get tweets of the user and those he follows
  def feed
    Tweet.where("user_id IN (?) OR user_id = ?", following_ids, id)
  end

  # Actual follow method
  def follow(a_user)
    followings << a_user
  end

  # Actual unfollow method
  def unfollow(a_user)
    followings.delete(a_user)
  end

  # Returns a list of users not followed by particular user
  # This method is called in the user index method
  def unfollowed_users
    User.where("id NOT IN (?)", following_ids)
  end
end
