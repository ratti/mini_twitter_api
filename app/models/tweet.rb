class Tweet < ActiveRecord::Base

  belongs_to :user

  validates :content, presence: true
  validates :user_id, presence: true

  scope :recent_first, -> { order(created_at: :desc) }

  def as_json(options = {})
    super(options.merge(include: :user))
  end

end
