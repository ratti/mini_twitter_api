# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

list_surnames   = %w(Boateng Ofori Okyere Amoah Agyei Koomson Mensah Bobbie Lartey Okan Lomotey Kwashie Banson Arthur)
list_othernames = %w(Francis Percy Pearl Opoku Dede Helen Akosua James Kofi Kwame Mark John Matthew Philip)
tweets = ["After Many a Summer Dies the Swan",
 "Beneath the Bleeding",
 "Beyond the Mexique Bay",
 "Brandy of the Damned",
 "Bury My Heart at Wounded Knee",
 "Butter In a Lordly Dish",
 "By Grand Central Station I Sat Down and Wept",
 "The Curious Incident of the Dog in the Night-Time",
 "Endless Night",
 "Everything is Illuminated",
 "Eyeless in Gaza",
 "Fair Stood the Wind for France",
 "Fame Is the Spur",
 "The Heart Is a Lonely Hunter",
 "The Heart Is Deceitful Above All Things",
 "I Know Why the Caged Bird Sings",
 "I Sing the Body Electric",
 "I Will Fear No Evil",
 "If I Forget Thee Jerusalem",
 "It's a Battlefield",
 "Let Us Now Praise Famous Men",
 "Lilies of the Field",
 "This Lime Tree Bower",
 "The Line of Beauty",
 "The Little Foxes",
 "Time of our Darkness",
 "The Way Through the Woods",
 "The Wealth of Nations",
 "What's Become of Waring",
 "When the Green Woods Laugh",
 "Where Angels Fear to Tread",
 "The Yellow Meads of Asphodel"]

User.create(name: "User Name", 
            username: "user", 
            email: "user@app.com",
            password: "12345678",
            password_confirmation: "12345678")

10.times do |n|
  fname = "#{list_othernames.sample}"
  lname = "#{list_surnames.sample}"
  username = "#{lname}#{(1..10).to_a.sample}"

  User.create(name: "#{fname} #{lname}",
              username: "#{username}",
              email: "#{username}@#{%w(gmail yahoo).sample}.com",
              password: "12345678",
              password_confirmation: "12345678")
end

users = User.all
5.times do
  users.each { |user| user.tweets.create!(content: tweets.sample) }
end